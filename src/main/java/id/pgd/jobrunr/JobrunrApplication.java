package id.pgd.jobrunr;

import org.jobrunr.scheduling.JobScheduler;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class JobrunrApplication {
	public static void main(String[] args) {
		SpringApplication.run(JobrunrApplication.class, args);
	}

	@Bean
	ApplicationRunner appRunner(JobScheduler jobScheduler) {
		return args -> jobScheduler.enqueue(() -> System.out.println("Hello world (first step with Jobrunr)"));
	}
}
